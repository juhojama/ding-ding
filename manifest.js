{
  "name": "Lattakone",
  "short_name": "Latta",
  "theme_color": "#ffffee",
  "background_color": "#ffffee",
  "display": "standalone",
  "Scope": "/",
  "start_url": "/",
  "icons": [
    {
      "src": "assets/img/sticker.png",
      "sizes": "72x72",
      "type": "image/png"
    },
    {
      "src": "assets/img/sticker.png",
      "sizes": "96x96",
      "type": "image/png"
    },
    {
      "src": "assets/img/sticker.png",
      "sizes": "128x128",
      "type": "image/png"
    },
    {
      "src": "assets/img/sticker.png",
      "sizes": "144x144",
      "type": "image/png"
    },
    {
      "src": "assets/img/sticker.png",
      "sizes": "152x152",
      "type": "image/png"
    },
    {
      "src": "assets/img/sticker.png",
      "sizes": "192x192",
      "type": "image/png"
    },
    {
      "src": "assets/img/sticker.png",
      "sizes": "384x384",
      "type": "image/png"
    },
    {
      "src": "assets/img/sticker.png",
      "sizes": "512x512",
      "type": "image/png"
    }
  ],
  "splash_pages": null
}
